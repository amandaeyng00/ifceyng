from config import *
from model import *

@app.route("/")
def home():
    return render_template("home.html")

@app.route("/about")
def about():
    return render_template("about.html")

@app.route("/listar_pessoas")
def listar_pessoas():
    with db_session:
        pessoas = Pessoa.select()
        return render_template("listar_pessoas.html", pessoas=pessoas)

@app.route("/form_adicionar_pessoa")
def form_adicionar_pessoa():
    return render_template("form_adicionar_pessoa.html")

@app.route("/adicionar_pessoa")
def adicionar_pessoa():
    nome = request.args.get("Nome")
    email = request.args.get("Email")
    telefone = request.args.get("Telefone")
    data_nasc = request.args.get("Data de Nascimento")
    tipo_sang = request.args.get("Tipo Sanguíneo")
    with db_session:
        p = Pessoa(**request.args)
        commit()
        return redirect("listar_pessoas")